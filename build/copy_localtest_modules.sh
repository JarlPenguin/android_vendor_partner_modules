if [[ -z $AOSP_ROOT ]];
then
  echo 'Error: $AOSP_ROOT is not defined. Please define it like $ export AOSP_ROOT=<path/to/aosp/repository/root>'
  exit -1
fi

if [[ -z $ANDROID_BUILD_TOP ]];
then
  echo 'Error: $ANDROID_BUILD_TOP is not defined. Please run envsetup.sh'
  exit -1
fi

AOSP_MODULE_OUT="$AOSP_ROOT/out/target/product/generic_arm64/system"
DEAPEXER="$AOSP_ROOT/out/host/linux-x86/bin/deapexer"
MODULES_DIR="$ANDROID_BUILD_TOP/vendor/partner_modules"
LOCAL_TEST_PREBUILTS_DIR="$MODULES_DIR/LocalTestPrebuilts"

if [[ -d $LOCAL_TEST_PREBUILTS_DIR ]];
then
  rm -rf $LOCAL_TEST_PREBUILTS_DIR
fi

mkdir -p $LOCAL_TEST_PREBUILTS_DIR

cat > $LOCAL_TEST_PREBUILTS_DIR/Android.bp <<EOF
android_app_import {
    name: "NetworkStackLocalTest",
    apk: "NetworkStackLocalTest.apk",
    presigned: true,
    privileged: true,
    overrides: ["NetworkStack", "NetworkStackNext",],
    required: ["NetworkStack_permissions.xml",],
}

prebuilt_etc {
    name: "NetworkStack_permissions.xml",
    src: "NetworkStack_permissions.xml",
    sub_dir: "permissions",
}

android_app_import {
    name: "NetworkPermissionConfigLocalTest",
    apk: "NetworkPermissionConfigLocalTest.apk",
    presigned: true,
    privileged: true,
    overrides: ["NetworkPermissionConfig",],
}

android_app_import {
    name: "CaptivePortalLoginLocalTest",
    apk: "CaptivePortalLoginLocalTest.apk",
    presigned: true,
    overrides: ["CaptivePortalLogin",],
}

prebuilt_apex {
    name: "com.android.resolv.localtest",
    overrides: ["com.android.resolv"],
    src: "com.android.resolv.localtest.apex",
    filename: "com.android.resolv.apex",
}

prebuilt_apex {
    name: "com.android.tethering.localtest",
    overrides: ["com.android.tethering"],
    src: "com.android.tethering.localtest.apex",
    filename: "com.android.tethering.apex",
    required: ["Tethering_permissions.xml",],
}

prebuilt_etc {
    name: "Tethering_permissions.xml",
    src: "Tethering_permissions.xml",
    sub_dir: "permissions",
}

prebuilt_apex {
    name: "com.android.cellbroadcast.localtest",
    overrides: ["com.android.cellbroadcast"],
    src: "com.android.cellbroadcast.localtest.apex",
    filename: "com.android.cellbroadcast.apex",
    required: [
        "CellBroadcast_permissions.xml",
        "CellBroadcast_config.xml",
    ],
}

prebuilt_etc {
    name: "CellBroadcast_permissions.xml",
    src: "CellBroadcast_permissions.xml",
    sub_dir: "permissions",
}
prebuilt_etc {
    name: "CellBroadcast_config.xml",
    src: "CellBroadcast_config.xml",
    product_specific: true,
    sub_dir: "sysconfig",
}
EOF

cp $AOSP_MODULE_OUT/priv-app/NetworkStack/NetworkStack.apk $LOCAL_TEST_PREBUILTS_DIR/NetworkStackLocalTest.apk
cp $AOSP_MODULE_OUT/priv-app/NetworkPermissionConfig/NetworkPermissionConfig.apk $LOCAL_TEST_PREBUILTS_DIR/NetworkPermissionConfigLocalTest.apk
cp $AOSP_MODULE_OUT/app/CaptivePortalLogin/CaptivePortalLogin.apk $LOCAL_TEST_PREBUILTS_DIR/CaptivePortalLoginLocalTest.apk
$DEAPEXER decompress --input $AOSP_MODULE_OUT/apex/com.android.tethering.capex --output $LOCAL_TEST_PREBUILTS_DIR/com.android.tethering.localtest.apex
$DEAPEXER decompress --input $AOSP_MODULE_OUT/apex/com.android.resolv.capex --output $LOCAL_TEST_PREBUILTS_DIR/com.android.resolv.localtest.apex
$DEAPEXER decompress --input $AOSP_MODULE_OUT/apex/com.android.cellbroadcast.capex --output $LOCAL_TEST_PREBUILTS_DIR/com.android.cellbroadcast.localtest.apex
sed -E 's/com.google.android.networkstack/com.android.networkstack/' $MODULES_DIR/NetworkStackPrebuilt/GoogleNetworkStack_permissions.xml > $LOCAL_TEST_PREBUILTS_DIR/NetworkStack_permissions.xml
sed -E 's/com.google.android.tethering/com.android.tethering/' $MODULES_DIR/TetheringPrebuilt/GoogleTethering_permissions.xml > $LOCAL_TEST_PREBUILTS_DIR/Tethering_permissions.xml
sed -E -e 's/com.google.android.cellbroadcastreceiver/com.android.cellbroadcastreceiver.module/' -e 's/com.google.android.cellbroadcastservice/com.android.cellbroadcastservice/' $MODULES_DIR/CellBroadcastPrebuilt/GoogleCellBroadcast_permissions.xml > $LOCAL_TEST_PREBUILTS_DIR/CellBroadcast_permissions.xml
sed -E 's/com.google.android.cellbroadcastreceiver/com.android.cellbroadcastreceiver.module/' $MODULES_DIR/CellBroadcastPrebuilt/GoogleCellBroadcast_config.xml > $LOCAL_TEST_PREBUILTS_DIR/CellBroadcast_config.xml
