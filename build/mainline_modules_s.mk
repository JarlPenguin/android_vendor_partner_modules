#
# Copyright 2021 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Mainline configuration for regular devices that
#   are not low RAM and
#   can support updatable APEX
#
# Flags for partners:
#   MAINLINE_INCLUDE_WIFI_MODULE := true or false
#   - when it is true, WiFi module will be added to PRODUCT_PACKAGES

# Mainline modules - APK type
PRODUCT_PACKAGES += \
    com.google.android.modulemetadata \
    DocumentsUIGoogle \
    CaptivePortalLoginGoogle \
    NetworkPermissionConfigGoogle \
    NetworkStackGoogle \
    com.google.mainline.telemetry \

# Ingesting networkstack.x509.pem
PRODUCT_MAINLINE_SEPOLICY_DEV_CERTIFICATES=vendor/partner_modules/NetworkStackPrebuilt

# Configure APEX as updatable
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# Mainline modules - APEX type
PRODUCT_PACKAGES += \
    com.google.android.tzdata3 \
    com.google.mainline.primary.libs \

# following modules are moved below to handle APEX compression
#    com.google.android.adbd_trimmed \
#    com.google.android.art \
#    com.google.android.cellbroadcast \
#    com.google.android.conscrypt \
#    com.google.android.extservices \
#    com.google.android.mediaprovider \
#    com.google.android.ipsec \
#    com.google.android.media \
#    com.google.android.media.swcodec \
#    com.google.android.neuralnetworks \
#    com.google.android.os.statsd \
#    com.google.android.permission \
#    com.google.android.resolv \
#    com.google.android.scheduling \
#    com.google.android.sdkext \
#    com.google.android.tethering \

# adding compressed APEX based on options

# Adbd
MAINLINE_COMPRESS_APEX_ADBD ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_ADBD),true)
PRODUCT_PACKAGES += \
    com.google.android.adbd_trimmed_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.adbd_trimmed
endif

# Art
MAINLINE_COMPRESS_APEX_ART ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_ART),true)
PRODUCT_PACKAGES += \
    com.google.android.art_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.art
endif

# CellBroadcast
MAINLINE_COMPRESS_APEX_CELLBROADCAST ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_CELLBROADCAST),true)
PRODUCT_PACKAGES += \
    com.google.android.cellbroadcast_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.cellbroadcast
endif

# Conscrypt
MAINLINE_COMPRESS_APEX_CONSCRYPT ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_CONSCRYPT),true)
PRODUCT_PACKAGES += \
    com.google.android.conscrypt_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.conscrypt
endif

# DNS Resolver
MAINLINE_COMPRESS_APEX_RESOLV ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_RESOLV),true)
PRODUCT_PACKAGES += \
    com.google.android.resolv_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.resolv
endif

# ExtServices - apex
MAINLINE_COMPRESS_APEX_EXTSERVICES ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_EXTSERVICES),true)
PRODUCT_PACKAGES += \
    com.google.android.extservices_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.extservices
endif

# Ipsec
MAINLINE_COMPRESS_APEX_IPSEC ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_IPSEC),true)
PRODUCT_PACKAGES += \
    com.google.android.ipsec_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.ipsec
endif

# Media
MAINLINE_COMPRESS_APEX_MEDIA ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_MEDIA),true)
PRODUCT_PACKAGES += \
    com.google.android.media_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.media
endif

# MediaProvider
MAINLINE_COMPRESS_APEX_MEDIAPROVIDER ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_MEDIAPROVIDER),true)
PRODUCT_PACKAGES += \
    com.google.android.mediaprovider_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.mediaprovider
endif

# MediaSwCodec
MAINLINE_COMPRESS_APEX_MEDIASWCODEC ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_MEDIASWCODEC),true)
PRODUCT_PACKAGES += \
    com.google.android.media.swcodec_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.media.swcodec
endif

# Neural Networks
MAINLINE_COMPRESS_APEX_NEURALNETWORKS ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_NEURALNETWORKS),true)
PRODUCT_PACKAGES += \
    com.google.android.neuralnetworks_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.neuralnetworks
endif

# Statsd
MAINLINE_COMPRESS_APEX_STATSD ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_STATSD),true)
PRODUCT_PACKAGES += \
    com.google.android.os.statsd_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.os.statsd
endif

# Permission
MAINLINE_COMPRESS_APEX_PERMISSION ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_PERMISSION),true)
PRODUCT_PACKAGES += \
    com.google.android.permission_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.permission
endif

# Scheduling
MAINLINE_COMPRESS_APEX_SCHEDULING ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_SCHEDULING),true)
PRODUCT_PACKAGES += \
    com.google.android.scheduling_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.scheduling
endif

# SdkExtensions
MAINLINE_COMPRESS_APEX_SDKEXT ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_SDKEXT),true)
PRODUCT_PACKAGES += \
    com.google.android.sdkext_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.sdkext
endif

# Tethering
MAINLINE_COMPRESS_APEX_TETHERING ?= true
ifeq ($(MAINLINE_COMPRESS_APEX_TETHERING),true)
PRODUCT_PACKAGES += \
    com.google.android.tethering_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.tethering
endif

# Optional WiFi module
MAINLINE_INCLUDE_WIFI_MODULE ?= false
MAINLINE_COMPRESS_APEX_WIFI ?= true
ifeq ($(MAINLINE_INCLUDE_WIFI_MODULE),true)
ifeq ($(MAINLINE_COMPRESS_APEX_WIFI),true)
PRODUCT_PACKAGES += \
    com.google.android.wifi_compressed
else
PRODUCT_PACKAGES += \
    com.google.android.wifi
endif
endif

# sysconfig files
PRODUCT_COPY_FILES += \
    vendor/partner_modules/build/google-staged-installer-whitelist.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google-staged-installer-whitelist.xml \
    vendor/partner_modules/build/google-rollback-package-whitelist.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google-rollback-package-whitelist.xml \
